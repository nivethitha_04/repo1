package net.codejava;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import org.bson.Document;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class MongoDBApp2{

	public static void main(String[] args) throws Exception  {
		String uri1=System.getenv("uri");
		FileReader reader=new FileReader("src/main/resources/Demo2.properties");
		Properties prop=new Properties();
		prop.load(reader);
		if(System.getenv("uri").equals("")){
			uri1=prop.getProperty("db.uri");
		}
		MongoClient mongoClient=MongoClients.create(uri1);
		MongoDatabase db=mongoClient.getDatabase("codejava");
		MongoCollection<Document> collection=db.getCollection("inventory");
		String filepath=System.getenv("filepath");
		if(System.getenv("filepath").equals("")) {
			filepath=prop.getProperty("db.filepath");
		}
		new GetCSV().writeCSV(filepath,collection);
		
	}
}