package net.codejava;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.opencsv.CSVWriter;

public class GetCSV {
	public static void writeCSV(String filepath,MongoCollection<Document> collection) throws IOException {
		FindIterable<Document> docs=collection.find();
		File file = new File(filepath);
	    FileWriter outputfile = new FileWriter(file);
	    CSVWriter writer = new CSVWriter(outputfile);
		int i=0;
		String headers[]=new String[1000];
		headers[0]="x";
		writer.writeNext(headers);
	    for(Document doc : docs) {
	    	String str[]=new String[1000];
	    	str[0]=Integer.toString((int) doc.get("x"));
	    	i++;
	    	writer.writeNext(str);
	    	if(i>=50) {
	    		break;
	    	}
	    }
	   
		writer.close();
	}
}
